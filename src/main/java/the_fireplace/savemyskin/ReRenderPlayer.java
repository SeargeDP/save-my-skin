package the_fireplace.savemyskin;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ReRenderPlayer extends RenderPlayer {

	public ReRenderPlayer(RenderManager renderManager) {
		super(renderManager);
	}
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return new ResourceLocation(SaveMySkin.MODID, String.format("%s.png", Minecraft.getMinecraft().getSession().getUsername()));
	}
}
