package the_fireplace.savemyskin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.relauncher.FMLInjectionData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
@Mod(modid=SaveMySkin.MODID, name=SaveMySkin.MODNAME, version=SaveMySkin.VERSION, clientSideOnly=true)
public class SaveMySkin {
	public static final String MODID = "savemyskin";
	public static final String MODNAME = "Save My Skin";
	public static final String VERSION = "2.0.0.1";
	@Instance(MODID)
	public static SaveMySkin instance;

	public static final File skindir = new File((File)FMLInjectionData.data()[6], "cachedskins/");

	@EventHandler
	public void preInit(FMLPreInitializationEvent event){
		try {
			Files.createDirectory(skindir.toPath());
		} catch (IOException e) {}
		saveMySkin();
		FMLCommonHandler.instance().bus().register(this);
		//((SimpleReloadableResourceManager)Minecraft.getMinecraft().getResourceManager()).reloadResourcePack(new DummyResourcePack());
		//MinecraftForge.EVENT_BUS.register(this);
	}
	public void saveMySkin(){
		String alias=Minecraft.getMinecraft().getSession().getUsername();
		File file = new File(skindir, alias+".png");
		if(!file.exists()){
			try{
				URL url = new URL(String.format("http://skins.minecraft.net/MinecraftSkins/%s.png", alias));
				InputStream is = url.openStream();
				file.createNewFile();
				OutputStream os = new FileOutputStream(file);

				byte[] b = new byte[2048];
				int length;

				while((length = is.read(b)) != -1){
					os.write(b, 0, length);
				}
				is.close();
				os.close();
			}catch(IOException e){
			}
		}else{
			try{
				URL url = new URL(String.format("http://skins.minecraft.net/MinecraftSkins/%s.png", alias));
				InputStream is = url.openStream();
				file.delete();
				file.createNewFile();
				OutputStream os = new FileOutputStream(file);

				byte[] b = new byte[2048];
				int length;

				while((length = is.read(b)) != -1){
					os.write(b, 0, length);
				}
				is.close();
				os.close();
			}catch(IOException e){
			}
		}
	}
	@SubscribeEvent
	public void onPlayerLogin(final ClientConnectedToServerEvent event) {
		(new Thread() {
			@Override
			public void run() {
				while (Minecraft.getMinecraft().getTextureManager() == null || Minecraft.getMinecraft().thePlayer == null){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
				}
				EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;
				TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
				ITextureObject textureObject = texturemanager.getTexture(player.getLocationSkin());

				if (textureObject == null) {
					textureObject = new ThreadDownloadImageData((File) null, String.format("http://skins.minecraft.net/MinecraftSkins/%s.png", Minecraft.getMinecraft().getSession().getUsername()), player.getLocationSkin(), new ImageBufferDownload());
					texturemanager.loadTexture(player.getLocationSkin(), textureObject);
				}
				try {
					TextureUtil.uploadTextureImage(textureObject.getGlTextureId(), ImageIO.read(new File(skindir, Minecraft.getMinecraft().getSession().getUsername()+".png")));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	/*@SubscribeEvent
	public void renderPlayer(RenderPlayerEvent.Pre event){
		ReflectionHelper.setPrivateValue(RenderManager.class, Minecraft.getMinecraft().getRenderManager(), new ReRenderPlayer(Minecraft.getMinecraft().getRenderManager()), "field_178637_m");
	}*/
}

