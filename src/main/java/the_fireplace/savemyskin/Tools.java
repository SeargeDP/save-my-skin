package the_fireplace.savemyskin;

import java.awt.image.BufferedImage;
import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.StringUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
@SideOnly(Side.CLIENT)
public class Tools {
	public static void uploadPlayerSkin(AbstractClientPlayer player, BufferedImage bufferedImage) {
        TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject textureObject = texturemanager.getTexture(player.getLocationSkin());

        if (textureObject == null) {
            textureObject = new ThreadDownloadImageData((File) null, String.format("http://skins.minecraft.net/MinecraftSkins/%s.png", Minecraft.getMinecraft().getSession().getUsername()), player.getLocationSkin(), new ImageBufferDownload());
            texturemanager.loadTexture(player.getLocationSkin(), (ITextureObject) textureObject);
        }

        replaceSkin(textureObject, bufferedImage);
    }
	private static void replaceSkin(ITextureObject object, BufferedImage image){
		TextureUtil.uploadTextureImage(object.getGlTextureId(), image);
	}
}
