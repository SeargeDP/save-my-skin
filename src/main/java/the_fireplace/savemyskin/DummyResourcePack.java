package the_fireplace.savemyskin;

import java.io.File;

import net.minecraft.client.resources.FileResourcePack;
import net.minecraftforge.fml.relauncher.FMLInjectionData;

public class DummyResourcePack extends FileResourcePack {

	public DummyResourcePack() {
		super(new File((File)FMLInjectionData.data()[6], "cachedskins/"));
	}
	@Override
	public String getPackName()
	{
		return SaveMySkin.MODID;
	}
}
